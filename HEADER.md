# Ansible Role for Fedora Postgresql

Role to initialize and prepare systemd service for a postgresql database at a specified location.

## Example playbook:

```yaml
- hosts: servers
  tasks:
    - name: Initialize postgresql
      include_role:
        name: fedora-postgresql
      vars:
        data_path: /var/lib/data
        unit_name: var-lib-data
```
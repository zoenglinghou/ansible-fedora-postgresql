# Ansible Role for Fedora Postgresql

Role to initialize and prepare systemd service for a postgresql database at a specified location.

## Example playbook:

```yaml
- hosts: servers
  tasks:
    - name: Initialize postgresql
      include_role:
        name: fedora-postgresql
      vars:
        data_path: /var/lib/data
        unit_name: var-lib-data
```
## Table of content

- [Default Variables](#default-variables)
  - [data_path](#data_path)
  - [unit_name](#unit_name)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### data_path

path to `postgresql` data directory.

#### Default value

```YAML
data_path: ''
```

#### Example usage

```YAML
data_path: /var/lib/data
```

### unit_name

name of the `systemd` unit. The unit would be named as `postgresql@<systemd_unit_name>.service`.

#### Default value

```YAML
unit_name: ''
```

#### Example usage

```YAML
unit_name: var-lib-data
```



## Dependencies

None.

## License

['BSD']

## Author

[Linghao Zhang](zoenglinghou.github.io)
